#!/usr/bin/env bash

STACK_BASE_NAME=awsugs-todo
BACKEND_DIR=backend
FRONTEND_DIR=frontend
ADDITIONAL_STACK_PARAMS=""

function buildBackend() {
    yarn install
    node_modules/gulp/bin/gulp.js default
    npm test
    yarn run build
}

function buildFrontend() {
    yarn install
    node_modules/bower/bin/bower install
    node_modules/gulp/bin/gulp.js build
}


############# Don't change anything below this line unless you know what you are doing. #############

set -o nounset
set -o errexit

if ! [[ "$1" =~ ^(prd|stg|dev)$ ]]; then
    echo "Usage: ./deploy.sh <ENVIRONMENT>"
    echo "ENVIRONMENT=[prd|stg|dev]"
    exit 1;
fi

DEPLOY_ENVIRONMENT=$1
STACK_NAME=${STACK_BASE_NAME}-${DEPLOY_ENVIRONMENT}


function msg() {
    echo ""
    echo "################################################################################################################################"
    echo $1
    echo "################################################################################################################################"
    echo ""
}

function fail() {
    msg "$1";
    exit 1;
}

cd ..
BASEDIR=$(pwd)

msg "Starting build for stack ${STACK_NAME}"

msg "Deleting target folder ..."
rm -rf ${BASEDIR}/target || fail "Could not delete target folder."

msg "Building backend ..."
cd ${BASEDIR}
mkdir -p target/backend
cd ${BASEDIR}/${BACKEND_DIR}
buildBackend
msg "Finished backend build."

cd ${BASEDIR}/cloudformation

msg "Checking for backend deployment bucket ..."
if aws cloudformation describe-stacks --stack-name ${STACK_NAME} &> /dev/null; then
    LAMBDA_BUCKET=$(aws cloudformation describe-stack-resources --stack-name ${STACK_NAME} --logical-resource-id LambdaBucket --query "StackResources[].PhysicalResourceId" --output text)
    if [[ ! -z ${LAMBDA_BUCKET} ]]; then
        msg "Using existing lambda deployment bucket with name: ${LAMBDA_BUCKET}"
    else
        fail "Stack ${STACK_NAME} exists but lambda deployment bucket is missing."
    fi
else
    msg "Performing initial stack deployment to create lambda deployment bucket."
    aws cloudformation deploy --template-file cfn-initial.yml --stack-name ${STACK_NAME} --capabilities CAPABILITY_IAM --parameter-overrides Environment=${DEPLOY_ENVIRONMENT} ProjectKey=${STACK_BASE_NAME} || fail "Could not perform initial stack update."
    LAMBDA_BUCKET=$(aws cloudformation describe-stack-resources --stack-name ${STACK_NAME} --logical-resource-id LambdaBucket --query "StackResources[].PhysicalResourceId" --output text)
    if [[ -z ${LAMBDA_BUCKET} ]]; then
        fail "Could not create lambda bucket, check the stack resources and messages in the AWS console."
    else
        msg "Created deployment bucket: ${LAMBDA_BUCKET}"
    fi
fi

msg "Deploying backend ..."
rm -f cfn.packaged.yml
aws cloudformation package --template-file cfn.yml --s3-bucket ${LAMBDA_BUCKET} --output-template-file cfn.packaged.yml || fail "Could not package lambda deployment bundle."
aws cloudformation deploy --template-file cfn.packaged.yml --stack-name ${STACK_NAME} --capabilities CAPABILITY_IAM --parameter-overrides Environment=${DEPLOY_ENVIRONMENT} ProjectKey=${STACK_BASE_NAME} ${ADDITIONAL_STACK_PARAMS} || fail "Could not perform stack update."
msg "Finished backend deployment."

export ADDON_BASE_URL=$(aws cloudformation describe-stacks --stack-name ${STACK_NAME} --query "Stacks[0].Outputs[?OutputKey == 'AddonBaseUrl'].OutputValue" --output text)

msg "Building frontend ..."
cd ${BASEDIR}
mkdir -p target/frontend
cd ${BASEDIR}/${FRONTEND_DIR}
buildFrontend
msg "Finished frontend build."

cd ${BASEDIR}/target

msg "Deploying frontend ..."
WEBAPP_BUCKET=$(aws cloudformation describe-stack-resources --stack-name ${STACK_NAME} --logical-resource-id WebappBucket --query "StackResources[].PhysicalResourceId" --output text)
if [[ -z ${WEBAPP_BUCKET} ]]; then
    fail "Could not determine webapp bucket."
fi
aws s3 sync --delete --exact-timestamps frontend/ s3://${WEBAPP_BUCKET} --cache-control max-age=3600 || fail "Could not upload frontend resources."
aws s3 cp frontend/index.html s3://${WEBAPP_BUCKET}/index.html || fail "Could not upload index.html file."
msg "Finished frontend deployment."

msg "Addon is available at: ${ADDON_BASE_URL}"
