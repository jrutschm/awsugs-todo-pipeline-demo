# Demonstration project for AWS serverless deployments with Bitbucket pipelines #

This project is based on https://github.com/taimos/awsugs-todo

When forking this repository make sure to add these environment variables to your pipeline config:

* ```AWS_ACCESS_KEY_ID``` - the ID of the access key for AWS login. Needs permission to set up all stack resources.
* ```AWS_SECRET_ACCESS_KEY``` - the secret part of the access key. Add this as a 'secure' environment variable.
* ```AWS_DEFAULT_REGION``` - the region your stack will be deployed to.

## Note ##
The very first deployment will take about an hour due to the CloudFront Distribution resource.
If you are on a bitbucket plan with limited build minutes you might therefore want to do the initial deployment from a local computer by calling the ```deploy.sh``` script inside the ```cloudformation``` folder like this:
````
cd cloudformation && ./deploy.sh dev
````