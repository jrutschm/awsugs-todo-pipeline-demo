(function () {
  'use strict';
  angular.module('app')
    .factory('TodoService', Service);
  
  /** @ngInject */
  function Service($http) {
    return {
      getList: function () {
        return $http.get('/api/todos').then(function (res) {
          return res.data;
        });
      },
      create: function (todo) {
        return $http.post('/api/todos', todo).then(function (res) {
          return res.data;
        });
      },
      update: function (todo) {
        return $http.put('/api/todos/' + todo.id, todo).then(function (res) {
          return res.data;
        });
      }
    };
  }
  
})();