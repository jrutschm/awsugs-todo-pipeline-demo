(function () {
  'use strict';
  
  angular
    .module('app')
    .component('pageFooter', {
      templateUrl: 'app/components/pageFooter/pageFooter.html'
    });
  
  
})();